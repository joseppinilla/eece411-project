package com.group8.eece411.KVStore;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentMap;

public class KVReplication {

	public static void copyNewReplicas(KVHashNodes kvNodes, ConcurrentMap<String, String> kvMap, int serverPort){
		
		InetAddress newSuccessor = null;
		try {
			newSuccessor = kvNodes.getNewSuccessorInetAddress();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		KVClient kvClient = new KVClient(newSuccessor, serverPort);
			
		for (ConcurrentMap.Entry<String, String> entry : kvMap.entrySet()){
			
			try {
				if (kvNodes.isKeyLocal(entry.getKey())){
					kvClient.replicate(entry.getKey(), (short)entry.getValue().length(), entry.getValue());
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void updateReplicas(String hostname, KVHashNodes kvNodes, ConcurrentMap<String, String> kvMap, int serverPort){
		
		KVClient kvClient = null;
		try {
			kvClient = new KVClient(InetAddress.getByName(hostname), serverPort);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		for (ConcurrentMap.Entry<String, String> entry : kvMap.entrySet()){
		    
			//If key falls in range of joined node, replicate it
			try {
				if (kvNodes.doesKeyBelongToHostname(entry.getKey(),hostname)){
					kvClient.replicate(entry.getKey(), (short)entry.getValue().length(), entry.getValue());
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}

			//If removing one coordinator, remove unused keys
			try {
				if (kvNodes.isKetOutOfRange(entry.getKey())){
					kvMap.remove(entry.getKey());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
		
	}	
}
