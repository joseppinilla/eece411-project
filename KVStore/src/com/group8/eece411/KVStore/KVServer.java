package com.group8.eece411.KVStore;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class KVServer{

	/*Request
	+=====+=====+=====+======+=========+
	| HDR | CMD | KEY | VLEN |   VAL   |
	+=====+=====+=====+======+=========+
	| 16B | 1B  | 32B | 2B   | <15000B |
	+-----+-----+-----+------+---------+
	 */

	public static final int HDR_SIZE = 16;
	public static final int CMD_SIZE = 1;
	public static final int KEY_SIZE = 32;
	public static final int VLEN_SIZE = 2;
	public static final int MAX_VAL_LEN = 15000;
	
	
	public static final int IDMEMB_SIZE = 8;
	public static final int LENMEMB_SIZE = 2;
	
	
	public static final int IDMEMB_INDEX = HDR_SIZE;
	public static final int LENMEMB_INDEX = IDMEMB_INDEX + IDMEMB_SIZE;
	public static final int MEMB_HN_INDEX = LENMEMB_INDEX + LENMEMB_SIZE;

	
	public static final int MAX_KEYS = 100000;

	public static final int CMD_INDEX = HDR_SIZE;
	public static final int KEY_INDEX = CMD_INDEX+CMD_SIZE;
	public static final int VLEN_INDEX = KEY_INDEX+KEY_SIZE;
	public static final int VAL_INDEX = VLEN_INDEX+VLEN_SIZE;

	public static int REQ_SIZE = HDR_SIZE+CMD_SIZE+KEY_SIZE+VLEN_SIZE+MAX_VAL_LEN;
	public static int REP_SIZE = HDR_SIZE+CMD_SIZE+KEY_SIZE+VLEN_SIZE+MAX_VAL_LEN;

	public static final int RELAXED_TIMEOUT = 400;
	public static final int KEEP_ID_TIME_MS = 1000;
		
	public static final int KVMAP_SIZE = 100000;
	public static ConcurrentMap<String, String> KVMap;	//Main Key, Value store map
	public static List<String> ActiveReqList;			//List of active request IDs to avoid serving request duplicates
	public static KVHashNodes KVnodes;					//Map of active nodes

	public static final int IDDEL_REQ_CACHE_SIZE = 4;		//Maximum size of Delete Node request IDs cache
	public static ConcurrentLinkedQueue<Long> DelNodeList;	//List of recent IDs of Delete Node requests	
	
	public static final int IDJOIN_REQ_CACHE_SIZE = 4;		//Maximum size of JoinNode request IDs cache
	public static ConcurrentLinkedQueue<Long> JoinNodeList;	//List of recent IDs of Join Node requests
	
	public static ConcurrentMap<String, Tuple<String,Long>> ReqKeyCache; //Request Key, Value Cache structure
	public static final int REQ_KEY_CACHE_SIZE = 4;		//Maximum size of cache
	public static final int CACHE_MAX_TIME_MS = 3000;	//Maximum time to use keys kept in cache

	public static final int GOSSIP_NUM = 3; 		//Number of nodes to write to on gossip protocol
	public static final int QUORUM_W = 2;
	public static final int QUORUM_R = 2;
	
	public static int REPLICAS_NUM = 2; 			//Number of nodes to write to on gossip protocol
	public static int MEDIAN = (REPLICAS_NUM/2)+1;	//Median or majority in array of size REPLICAS_NUm
	
	public static final int CHECK_TIME = 10000;		//Period to check if a random node is alive 
	
	private short serverPort;

	/** 
	 * KVStore Server constructor, configurable retry timeout
	 */
	public KVServer (short serverPort){

		//Store Datagram port
		this.serverPort = serverPort;

		// Create Hash Map
		KVMap =  new ConcurrentHashMap<String, String>(KVMAP_SIZE);

		// Create List of active requests
		ActiveReqList = new CopyOnWriteArrayList<String>();
		
		// Create List for Delete Node requests Caching
		DelNodeList = new LimitedSizeQueue<Long>(IDDEL_REQ_CACHE_SIZE);
		
		// Create List for join Node requests Caching
		JoinNodeList = new LimitedSizeQueue<Long>(IDJOIN_REQ_CACHE_SIZE);

		// Create Map for Request Key,Value Caching 
		ReqKeyCache = new ConcurrentHashMap<String,Tuple<String,Long>>(REQ_KEY_CACHE_SIZE);	
	
		//Deploy without replicas
		if(!KVServerRun.DEPLOY_MODE){
			REPLICAS_NUM = 0;
			MEDIAN = 0;
		}
		
		//Create Map of Node Membership
		try {
			KVnodes = new KVHashNodes("serviceNodes.txt",REPLICAS_NUM);
		} catch (IOException e) {
			if(KVServerRun.DEV_MODE)
				System.out.println("IOException in Create Map of Node Membership");
			e.printStackTrace();
		}
	}

	/** 
	 * KVStore Server main loop
	 * Wait for request, spawn thread on receive
	 */
	public void runServer() {
		DatagramSocket serverSocket = null;

		try {
			serverSocket = new DatagramSocket(serverPort);
			if(KVServerRun.DEV_MODE){
				System.out.println("Server initiated on port " + serverPort);
			}

		} catch (SocketException e) {
			System.err.println("Failed Opening Socket");
			System.exit(1);
		}

		byte[] reqBuf = new byte[REQ_SIZE];
		DatagramPacket receivePacket = new DatagramPacket(reqBuf,REQ_SIZE);

		// Before going into main server loop spread join event
		gossipJoinRequest(serverPort);
		
		// Schedule timed task to check nodes
		Timer timer = new Timer();
		timer.schedule(new checkNode(), CHECK_TIME, CHECK_TIME);
				
		// Main Server Loop. Launch new thread on request
		while(true) {
			reqBuf = new byte[REQ_SIZE];

			receivePacket.setData(reqBuf);

			try {
				serverSocket.receive(receivePacket);
			} catch (IOException e) {
				serverSocket.close();
				e.printStackTrace();
			}

			new Thread(new KVThread(serverSocket, receivePacket,serverPort,REP_SIZE)).start();
		}
	}
	
	/**
	 * Scheduled thread pings random node, if relaxed communication fails it send a gossip delete node request
	 */
	class checkNode extends TimerTask {
	    public void run() {
	    	
	    	InetAddress randNode = null;
			try {
				randNode = KVnodes.getRandomNodeInetAddress();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	
	    	KVClient kvClient = new KVClient(randNode, serverPort, RELAXED_TIMEOUT);
	    	
	    	String randNodeHostname = randNode.getHostName();
	    	
	    	if(KVServerRun.DEV_MODE){
	    		System.out.println("PING REQUEST TO: " + randNodeHostname);
			}
	    	
	    	if(kvClient.ping()!=KVErrors.OK){
	    		if(KVnodes.deleteNodeByHostname(randNodeHostname) != null){
	    			gossipDeleteRequest(randNodeHostname, serverPort);
	    		}
	    	}else{ //If node did not reply
	    		if(KVnodes.joinNodeByHostname(randNodeHostname) == null){
	    			gossipJoinRequest(randNodeHostname, serverPort);
	    		}
	    	}
	    }
	 }
	
	/** 
	 * KVStore Server thread
	 * Launched on request receive, retrieves and decode request, finds owner of key.
	 * If key is local then reply
	 * If key is remote then forward request
	 */	
	static class KVThread implements Runnable {

		DatagramSocket serverSocket = null;
		byte[] packetData = null;
		InetAddress packetAddress = null;
		int packetPort = 0;
		ByteBuffer repBuf = null;
		short valLength = 0;
		String reqVal = null;
		String reqKey = null;
		KVErrors errKV = null;
		KVCmds cmdKV = null;
		Byte cmdByte;
		Boolean shutDown = false;
		int serverPort = 0;

		/**
		 * Thread constructor
		 * @param serverSocket
		 * @param receivePacket
		 * @param serverPort
		 * @param bufSize
		 */
		public KVThread(DatagramSocket serverSocket, DatagramPacket receivePacket, int serverPort, int bufSize) {
			this.serverSocket = serverSocket;
			this.serverPort = serverPort; 
			packetData = Arrays.copyOfRange(receivePacket.getData(),0,receivePacket.getLength());
			packetAddress = receivePacket.getAddress();
			packetPort = receivePacket.getPort();
			repBuf = ByteBuffer.allocate(bufSize);
			repBuf.order(ByteOrder.LITTLE_ENDIAN);
		}

		/**
		 * Runnable thread. Parse received packet and serve request
		 */
		public void run() {

			//Read Request ID
			byte[] reqID = Arrays.copyOfRange(packetData,0,KVServer.HDR_SIZE);

			//Make sure the request is only serviced once
			String reqIDStr = null;
			reqIDStr = new String(reqID);

			//On replica-type requests don't verify connection
			boolean replicaType = false;
			
			if(!KVServer.ActiveReqList.contains(reqIDStr)){
				KVServer.ActiveReqList.add(reqIDStr);
			}else{
				return;
			}

			//Read command
			cmdByte = packetData[KVServer.HDR_SIZE];

			try{
				cmdKV = KVCmds.valueOf(cmdByte);
				errKV = KVErrors.OK;
			}catch (IndexOutOfBoundsException e){
				errKV = KVErrors.INVALID_CMD;
			}

			boolean localKey=true;

			if (((cmdKV == KVCmds.GET)||(cmdKV == KVCmds.SET)||(cmdKV == KVCmds.REMOVE))){
				//Read Key
				reqKey = new String(Arrays.copyOfRange(packetData,KEY_INDEX,VLEN_INDEX)).trim();

					//Check if key belongs to local node
					try {
						localKey = KVnodes.isKeyLocal(reqKey);
					} catch (UnknownHostException e) {
						e.printStackTrace();
						errKV = KVErrors.INTERNAL_ERROR;
					}
			}

			//Start filling reply buffer with reply ID
			repBuf.put(reqID);

			if(localKey){

				//If command is valid
				if (errKV.isOK()){

					//Depending on command
					switch (cmdKV){

					case SET:

						valLength = (short) ByteOrderPlus.leb2int((Arrays.copyOfRange(packetData,VLEN_INDEX,VAL_INDEX)),0,2);
						reqVal = new String(Arrays.copyOfRange(packetData,VAL_INDEX,VAL_INDEX + valLength)).trim();

						if(KVServerRun.DEV_MODE){
							System.out.println("SET REQUEST " + reqKey + " " + valLength + " " + reqVal);
						}
						
						if(valLength > MAX_VAL_LEN){
							errKV = KVErrors.INVALID_LENGTH;
						}else{
							//Write to replicas and get acceptance Quorum
							if(replicateKey()){
								if(KVServer.KVMap.size() >= MAX_KEYS){
									errKV = KVErrors.NO_SPACE;							
								}else{
									try{
										KVServer.KVMap.put(reqKey, reqVal);
									}catch (Exception e) {
										errKV = KVErrors.INTERNAL_ERROR;
									}
								}					
							}else{
								errKV = KVErrors.INTERNAL_ERROR;
							}	
						}

						//Put Response Code
						repBuf.put(errKV.getId());
						break;

					case GET:
												
						// Read cached values for requested key
						Tuple<String,Long> ReqTuple = ReqKeyCache.get(reqKey);
						if (ReqTuple != null){
							// If value is not stale
							if ((ReqTuple.y - System.currentTimeMillis()) < CACHE_MAX_TIME_MS){
								reqVal = ReqTuple.x;
								if (reqVal == null){
									errKV = KVErrors.NO_KEY;
								}
							}
						} else{
							
							if(!KVServerRun.DEPLOY_MODE){
								reqVal = KVServer.KVMap.get(reqKey);
								
								if(KVServerRun.DEV_MODE){
									System.out.println("GET REQUEST " + reqKey + " " + reqVal);
								}
								
								//Put Response Code
								repBuf.put(errKV.getId());
								
								if (reqVal != null){
									//Put Value-Length
									repBuf.putShort((short)reqVal.length());
									//Put Value
									repBuf.put(reqVal.getBytes());							
								}
								
								break;
							}
							
							
							// Get replicas with Quorum R
							errKV = getKeyReplicas(reqKey);
							
							//Put Response Code
							repBuf.put(errKV.getId());
							
							if (reqVal != null){
								//Put Value-Length
								repBuf.putShort((short)reqVal.length());
								//Put Value
								repBuf.put(reqVal.getBytes());							
							}
						}
						
						if(KVServerRun.DEV_MODE){
							System.out.println("GET REQUEST " + reqKey + " " + reqVal);
						}
						
						break;

					case REMOVE:
						if(KVServerRun.DEV_MODE){
							System.out.println("REMOVE REQUEST " + reqKey);
						}
						// Remove all key replicas
						errKV = removeKeyReplicas();
						//Put Response Code
						repBuf.put(errKV.getId());
						
						if (errKV == KVErrors.OK){
							reqVal = KVServer.KVMap.remove(reqKey);	
						}
						break;

					case KILL:
						if(KVServerRun.DEV_MODE){
							System.out.println("KILL REQUEST");
						}
						//Put Response Code OK
						repBuf.put(errKV.getId());
						shutDown = true;
						break;

					case DELNODE:
						//Get ID_DEL to compare against cached delete node requests
						long idDel = (long) ByteOrderPlus.leb2long((Arrays.copyOfRange(packetData,IDMEMB_INDEX,LENMEMB_INDEX)),0,8);
						
						if(KVServerRun.DEV_MODE){
							System.out.println("DELETE NODE REQUEST " + idDel);
						}
						
						if(DelNodeList.contains(idDel)){
							return;
						}
						else{
							//Get length of hostname value
							valLength = (short) ByteOrderPlus.leb2int((Arrays.copyOfRange(packetData,LENMEMB_INDEX,MEMB_HN_INDEX)),0,2);
							
							//Get hostname
							String delHostname;
							delHostname = new String(Arrays.copyOfRange(packetData,MEMB_HN_INDEX,MEMB_HN_INDEX+valLength)).trim();

							//If Delete is local
							//TODO: Wait some time. Send Join Request

							boolean deletingSuccessor = false;
							try {
								deletingSuccessor = KVnodes.isSuccessor(delHostname);
							} catch (IOException e) {
								e.printStackTrace();
							}
							//Delete Node in local KVHashNodes
							KVnodes.deleteNodeByHostname(delHostname);
							
							//Forward Delete node request in gossip protocol
							forwardGossip(packetData, this.serverPort);
							
							//Add request ID to cached list
							DelNodeList.add(idDel);
							
							//If node being deleted is successor, copy out all keys to next available node
							if (deletingSuccessor){
								KVReplication.copyNewReplicas(KVnodes, KVMap, serverPort);
							}
							
							return;
						}
					
					case JOINNODE:
						
						//Get ID_JOIN to compare against cached join node requests
						long idJoin = (long) ByteOrderPlus.leb2long((Arrays.copyOfRange(packetData,IDMEMB_INDEX,LENMEMB_INDEX)),0,8); 
						
						if(KVServerRun.DEV_MODE){
							System.out.println("JOIN NODE REQUEST " + idJoin);
						}						
						
						if(JoinNodeList.contains(idJoin)){
							return;
						}
						else{
							//Get length of hostname value
							valLength = (short) ByteOrderPlus.leb2int((Arrays.copyOfRange(packetData,LENMEMB_INDEX,MEMB_HN_INDEX)),0,2);
												
							//Get hostname
							String joinHostname = new String(Arrays.copyOfRange(packetData,MEMB_HN_INDEX,MEMB_HN_INDEX+valLength)).trim();

							//Join Node in local KVHashNodes
							KVnodes.joinNodeByHostname(joinHostname);
							
							//Forward Join node request in gossip protocol
							forwardGossip(packetData, this.serverPort);
							
							//Add request ID to cached list
							JoinNodeList.add(idJoin);						
							
							//If node joining is successor, copy out local keys to it and verify if coordinator changed
							try {
								if (KVnodes.isPredecessor(joinHostname)||KVnodes.isSuccessor(joinHostname)){
									KVReplication.updateReplicas(joinHostname,KVnodes,KVMap,serverPort);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
													
							return;
						}

					case REPLICATE:
						valLength = (short) ByteOrderPlus.leb2int((Arrays.copyOfRange(packetData,KVServer.VLEN_INDEX,KVServer.VAL_INDEX)),0,2);
						reqVal = new String(Arrays.copyOfRange(packetData,KVServer.VAL_INDEX,KVServer.VAL_INDEX + valLength)).trim();
						
						if(KVServer.KVMap.size() >= MAX_KEYS){
							errKV = KVErrors.NO_SPACE;							
						}else{
							try{
								KVServer.KVMap.put(reqKey, reqVal);
							}catch (Exception e) {
								errKV = KVErrors.INTERNAL_ERROR;
							}
						}
						//Put Response Code
						repBuf.put(errKV.getId());
						
						//Avoid connection check on internal failure
						replicaType = true;
						break;
						
					case GETREPLICA:
						reqVal = KVServer.KVMap.get(reqKey);
						if (reqVal!=null){
							//Put Response Code
							repBuf.put(errKV.getId());
							//Put Value-Length
							repBuf.putShort((short)reqVal.length());
							//Put Value
							repBuf.put(reqVal.getBytes());							
						}else{
							errKV = KVErrors.NO_KEY;
							//Put Response Code
							repBuf.put(errKV.getId());
						}
						//Avoid connection check on internal failure
						replicaType = true;
						
						break;
					case REMREPLICA: 
						reqVal = KVServer.KVMap.remove(reqKey);
						
						if (reqVal == null){
							//Key does not exist
							errKV = KVErrors.NO_KEY;
						}
						//Put Response Code
						repBuf.put(errKV.getId());
							
						//Avoid connection check on internal failure
						replicaType = true;
						
						break;
						
					case PING:
						//Put Response Code
						repBuf.put(errKV.getId());
					
					default:
						errKV = KVErrors.INVALID_CMD;
						break;
					}
				}
			}
			//Not Local
			else{ //
				
				KVClient kvClient = new KVClient(KVnodes.getNodeInetAddress(reqKey), this.serverPort);
				
				switch (cmdKV){
				
				case SET:
				case REMOVE://Forward set request as is
							errKV = kvClient.fwdRequest(packetData);
							//Put Response Code
							repBuf.put(errKV.getId());
							
							break;
					
				case GET:	Tuple<String,Long> ReqTuple = ReqKeyCache.get(reqKey);
							if (ReqTuple != null){
								// If value is not stale
								if ((ReqTuple.y - System.currentTimeMillis()) < CACHE_MAX_TIME_MS){
									reqVal = ReqTuple.x;
									if (reqVal == null){
										errKV = KVErrors.NO_KEY;
									}
								}
							}else{
								//Forward get request as is and parse response
								errKV = kvClient.fwdRequest(packetData);
								if (errKV==KVErrors.OK){
									reqVal = kvClient.parseGet();
								}
							}

							//Put Response Code
							repBuf.put(errKV.getId());
							
							if (errKV == KVErrors.OK){
								//Put Value-Length
								repBuf.putShort((short)reqVal.length());
								//Put Value
								repBuf.put(reqVal.getBytes());
							}
							
							break;
				
				default:
					errKV = KVErrors.INVALID_CMD;
					break;
				
				}
				
			}

			if(KVServerRun.DEV_MODE){
				System.out.println("REP PACKET " + Arrays.toString(Arrays.copyOfRange(repBuf.array(),0,repBuf.position())));
			}

			// Send reply to Client
			DatagramPacket sendPacket = new DatagramPacket(repBuf.array(), repBuf.position(), packetAddress, packetPort);			

			try {
				serverSocket.send(sendPacket);
			} catch (IOException e) {
				serverSocket.close();
				System.err.println("Failed on Reply");
				e.printStackTrace();
			}
			repBuf.clear();

			// Post-processing of requests
			if (shutDown){
				System.out.println("System Shut Down Request from Client" + packetAddress);
				System.exit(0);
			}

			// Only check aliveness of node if request was made remotely and not for a replica type request
			if ((!localKey)&&(!replicaType)){

				if (errKV==KVErrors.INTERNAL_ERROR){
					
					//Keep trying with more relaxed timeout for same node before deciding to delete
					KVClient kvClient = new KVClient(KVnodes.getNodeInetAddress(reqKey), this.serverPort, RELAXED_TIMEOUT);
					kvClient.ping();
					
					//If node doesn't reply then delete
					if(kvClient.errorKV != KVErrors.OK){
						String delHostname = KVnodes.getNodeHostname(reqKey);
						gossipDeleteRequest(delHostname, this.serverPort);
					}
				}
			}
			
			//If value read correctly. Add Key,Value to request cache				
			if (errKV==KVErrors.OK){
				ReqKeyCache.put(reqKey,new Tuple<String,Long>(reqVal,System.currentTimeMillis()));
			}

			//Thread doesn't remove request of list until after T. This covers the max time for retry.
			try {
				Thread.sleep(KEEP_ID_TIME_MS);
			} catch (InterruptedException e) {
				System.err.println("Thread Interrupted");
			}

			KVServer.ActiveReqList.remove(reqIDStr);

		}

		/**
		 * Replicate Key,Value on successor nodes
		 * @param Key
		 * @param Value Length
		 * @param Value
		 */
		private boolean replicateKey() {
			ArrayList<InetAddress> hostnames = null;

			
			if(!KVServerRun.DEPLOY_MODE){
				return true;
			}
			
			int achieveW = 0;
			
			if(KVServerRun.DEV_MODE){
				System.out.println("REPLICATING");
			}
			
			try {
				hostnames = KVnodes.getSuccessorsInetAddresses();
			} catch (Exception e) {
				e.printStackTrace();
			}

			KVClient kvClient = new KVClient(null, 0);
			
			for (int i=0;i<hostnames.size();i++){

				kvClient.setServer(hostnames.get(i), serverPort);
				kvClient.replicate(reqKey,valLength,reqVal);
				if (kvClient.errorKV==KVErrors.OK){
					achieveW+=1;
				}
			}
			if (achieveW>=QUORUM_W){
				return true;
			} else{
				return false;
			}
			
		}
		
		/**
		 * Removes all Replicated Key,Value on successor nodes
		 * @param Key
		 */
		private KVErrors removeKeyReplicas() {
			ArrayList<InetAddress> hostnames = null;
					
			int achieveW = 0;
			int noKey = 0;
			
			if(KVServerRun.DEV_MODE){
				System.out.println("REMOVING REPLICAS");
			}
			
			try {
				hostnames = KVnodes.getSuccessorsInetAddresses();
			} catch (Exception e) {
				e.printStackTrace();
			}

			KVClient kvClient = new KVClient(null, 0);
			
			for (int i=0;i<hostnames.size();i++){

				kvClient.setServer(hostnames.get(i), serverPort);
				kvClient.removeReplica(reqKey);
				if (kvClient.errorKV==KVErrors.OK){
					achieveW += 1;
				}else if(kvClient.errorKV==KVErrors.NO_KEY){
					noKey += 1;
				}
			}
			
			if ((achieveW+noKey) >= QUORUM_W){
				
				if (achieveW>noKey){
					errKV = KVErrors.OK;
				}else{
					errKV = KVErrors.NO_KEY;
				}
				
			} else{
				errKV = KVErrors.INTERNAL_ERROR;
			}
			
			return errKV;			
		}
		
		/**
		 * Get Key,Value replicas from successor nodes
		 * @param Key
		 */
		private KVErrors getKeyReplicas(String reqKey) {
			ArrayList<InetAddress> hostnames = null;
		
			List<String> kvReplicas = new ArrayList<String>();;
			kvReplicas.add(KVServer.KVMap.get(reqKey));
			
			int achieveR = 0;
			
			if(KVServerRun.DEV_MODE){
				System.out.println("GETTING REPLICAS");
			}
			
			try {
				hostnames = KVnodes.getSuccessorsInetAddresses();
			} catch (Exception e) {
				e.printStackTrace();
			}

			KVClient kvClient = new KVClient(null, 0);
			
			for (int i=0;i<hostnames.size();i++){

				kvClient.setServer(hostnames.get(i), serverPort);
				kvReplicas.add(kvClient.getReplica(reqKey));
				if (kvClient.errorKV==KVErrors.OK){
					achieveR+=1;
				}
			}
			
			if (achieveR >= QUORUM_R){
				
				java.util.Collections.sort(kvReplicas);
				reqVal = kvReplicas.get(MEDIAN);
				if (reqVal==null){
					errKV = KVErrors.NO_KEY;
				}else{
					errKV = KVErrors.OK;	
				}
				
			} else{
				errKV = KVErrors.INTERNAL_ERROR;
			}
			
			return errKV;
			
		}
		
		
	}
	
	/**
	 * Forward Gossip request to X random nodes using non blocking commands
	 * @param unchanged packet to forward
	 */
	private static void forwardGossip(byte[] packet, int serverPort) {
		
		ArrayList<InetAddress> hostnames = null;
		
		try {
			hostnames = KVnodes.getRandomMembersInetAddress(GOSSIP_NUM);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		KVClient kvClient = new KVClient(null, 0);
		
		for (int i=0;i<hostnames.size();i++){
			if(KVServerRun.DEV_MODE){
				System.out.println(i + " " + hostnames.get(i));
			}
			kvClient.setServer(hostnames.get(i), serverPort);
			kvClient.fwdNonBlockingRequest(packet);
		}
	}
	
	/**
	 * Send Gossip Delete Node request to X random nodes using non blocking commands
	 * @param Hostname of node to delete
	 */
	private static void gossipDeleteRequest(String hostname, int serverPort) {
		if(KVServerRun.DEV_MODE){
			System.out.println("SEND GOSSIP DELETE REQ");
		}
		gossipRequest(hostname, KVCmds.DELNODE, serverPort);
	}	

	
	/**
	 * Send Gossip Join Node request to X random nodes using non blocking commands
	 */
	private static void gossipJoinRequest(String hostname, int serverPort) {
		gossipRequest(hostname, KVCmds.JOINNODE, serverPort);
	}
	
	/**
	 * Send Gossip Own Join Node request to X random nodes using non blocking commands
	 */
	private static void gossipJoinRequest(int serverPort) {
		if(KVServerRun.DEV_MODE){
			System.out.println("SEND GOSSIP JOIN REQ " + serverPort);
		}
		String myHostname = null;

		try {
			myHostname = KVHashNodes.getMyHostname();
		} catch (UnknownHostException e) {
			System.exit(0);
			e.printStackTrace();
		}
		
		gossipRequest(myHostname, KVCmds.JOINNODE, serverPort);
	}	
	
	
	/**
	 * Send Gossip request to X random nodes using non blocking commands
	 * @param Data to send on gossip
	 */
	private static void gossipRequest(String data, KVCmds cmd, int serverPort) {
		
		ArrayList<InetAddress> hostnames = null;
		
		try {
			hostnames = KVnodes.getRandomMembersInetAddress(GOSSIP_NUM);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		KVClient kvClient = new KVClient(null, serverPort);
		kvClient.initGossip(data, cmd, serverPort);
		
		for (int i=0;i<hostnames.size();i++){
			if(KVServerRun.DEV_MODE){
				System.out.println(i + " " + hostnames.get(i));
			}
			kvClient.setServer(hostnames.get(i), serverPort);
			kvClient.sendNonBlockingRequest();
		}
	}
	
}