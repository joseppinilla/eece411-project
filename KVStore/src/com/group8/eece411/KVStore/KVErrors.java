package com.group8.eece411.KVStore;


public enum KVErrors {
	OK( 0,"Operation was Succesful."),
	NO_KEY( 1," Non-existent key requested in a get or delete operation."),
	NO_SPACE( 2,"Out of space."),
	OVERLOAD( 3,"System Overload."), //TODO: Recognize error
	INTERNAL_ERROR( 4,"Internal KVStore failure."),
	INVALID_CMD( 5,"Unrecognized command."),
	INVALID_LENGTH( 33,"Value Length is not valid (>15000).");


	private final int id;
	private final String message;
	
	private KVErrors(int id, String message){
		this.id = id;
		this.message = message;

	}

	public boolean isOK() {
		if(this.id==0){
			return true;
		}
		return false;
	}

	public byte getId() {
		return (byte )id;
	}
	
	public String getMessage() {
		return message;
	}

	public static KVErrors valueOf(byte b) {
		return KVErrors.values()[b];
	}
	
}