package com.group8.eece411.KVStore;

import static java.util.Arrays.asList;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class KVTester {
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static Random rnd = new Random();
	short serverPort = 5630;
	int timeOutms = 200;
	Random rand = new Random();
	
	public static void main(String args[]) throws IOException  {


		
			
//		/* Parse Options
//		 * */
//		
//		InetAddress serverIP = InetAddress.getByName("planetlab2.cs.ubc.ca");
//	
//		
//		
//		
		/* Protocol Layer creates packet and fills with UniqueUD
		 * */
//		short serverPort = 5630;
//		int timeOutms = 200;
//		KVClient kvStore = new KVClient(serverIP, serverPort,timeOutms);		
//			String randomKey = "QA6ZPR8EWLG5ZRX";
//			System.out.println(kvStore.get(randomKey));


		KVTester A = new KVTester();
		A.test1("test/assi5Nodes/assi5Nodes_1.txt", 5, false);
//		A.test1("test/assi4Nodes/assi4Nodes_1.txt", 20, true);
			
//		A.testOneSetFewGets("test/assi4Nodes/assi4Nodes_1.txt", 8, false);
//		A.testFewGets("test/assi4Nodes/assi4Nodes_1.txt", "8HRI55TO8Y0W4ID", 8, false);
		//8HRI55TO8Y0W4ID

	}  

	/*B.a Test objective - verify correctness of the replication functionality with failures
	 * Put requests for 1000 key-value pairs are randomly issued to arbitrary nodes (form a fixed list of nodes).
	 * One node is taken down - to simulate node failure. Wait 1 minute.
	 * Get requests for the keys used in step 1.  Expectation is that no data is lost.
	 * Repeat from step 1, until there is only one active node.
	 * data will contain (Client hostname for SET, key, value, returned info from SET, , client hostname for GET, returned value from GET)
	 */
	void test1(String filename, int numOfIterations, boolean killNode) throws IOException
	{
		
		//Get list of nodes
		ArrayList<String> nodes = readListOfNodes(filename);
	
		String[][] data = new String[1000][10];
		int randomNodeNumber;
		String randomNode;
		InetAddress serverIP;
		KVClient kvStore;
		
		//Send SET requests
		for(int i=0; i<numOfIterations; i++)
		{
			//Update Console with Progress
			System.out.println(i + "...");
			
			//Create a client for a random node
			randomNodeNumber = rand.nextInt(nodes.size());
			randomNode = nodes.get(randomNodeNumber);	
			serverIP = InetAddress.getByName(randomNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			data[i][0] = randomNode;
			
			//Generate random keys and values
			String randomKey 	= randomString(15);
//			String randomKey 	= "1";
			String randomValue 	= randomString(20);
			data[i][1] = randomKey;
			data[i][2] = randomValue;
			
			
			
			//Send request to client
			KVErrors reply = kvStore.set(randomKey, randomValue);
			data[i][3] = String.valueOf(reply);
			
			//Clear Console
		}

		//Kill node afte Set requests?
		if(killNode)
		{
			//Remove One Node
			randomNodeNumber = rand.nextInt(nodes.size());
			String deletedNode = nodes.remove(randomNodeNumber);
			//Kill the removed node
			serverIP = InetAddress.getByName(deletedNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			kvStore.kill();
		}
		
		
		//Send GET requests 1
		for(int i=0; i<numOfIterations; i++)
		{
			//Update Console with Progress
			System.out.println(i + "...");
			
			//Create a client for a random node
			randomNodeNumber = rand.nextInt(nodes.size());
			randomNode = nodes.get(randomNodeNumber);	
			serverIP = InetAddress.getByName(randomNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			data[i][4] = randomNode;
			
			//What's the Key?
			String key 	= data[i][1];

			
			//Send GET request to clients
			long startTime = System.nanoTime();
			String reply = null;
//			try{
				reply = kvStore.get(key);
//			}catch(Exception e){
//				reply = "Exception!";
//			}
			long estimatedTime = System.nanoTime() - startTime;
			if(reply!= null)
				data[i][5] = reply;
			else
				data[i][5] 	= kvStore.errorKV.getMessage();
			data[i][6] 		= String.valueOf(estimatedTime);
			
			//Clear Console
		}
		
		//Send GET requests 2
		for(int i=0; i<numOfIterations; i++)
		{
			//Update Console with Progress
			System.out.println(i + "...");
			
			//Create a client for a random node
			randomNodeNumber = rand.nextInt(nodes.size());
			randomNode = nodes.get(randomNodeNumber);	
			serverIP = InetAddress.getByName(randomNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			data[i][7] = randomNode;
			
			//What's the Key?
			String key 	= data[i][1];

			
			//Send GET request to clients
			long startTime = System.nanoTime();
			String reply = null;
			try{
				reply = kvStore.get(key);
			}catch(Exception e){
				reply = "Exception!";
			}
			long estimatedTime = System.nanoTime() - startTime;
			if(reply!= null)
				data[i][8] = reply;
			else
				data[i][8] 	= kvStore.errorKV.getMessage();
			data[i][9] 		= String.valueOf(estimatedTime);
			
			//Clear Console
		}
		//Print All Captured Data
		String[] columns = {"Node used for SET", "Key", "Value", "Set Reply", "Node used for Get1", "Get Reply1", "Execution Time", "Node used for Get2", "Get Reply2", "Execution Time"};
		for(int i=0; i<columns.length; i++)
			System.out.print(String.format("%1$-22s\t", columns[i]));
		System.out.println();
		for(int i=0; i<numOfIterations; i++)
		{
			for(int j=0; j<10; j++)
				System.out.print(String.format("%1$-22s\t", data[i][j]));
			System.out.println();
		}
		
	}
	
	
	void testOneSetFewGets(String filename, int numOfIterations, boolean killNode) throws IOException
	{
		
		//Get list of nodes
		ArrayList<String> nodes = readListOfNodes(filename);
	
		String[] dataSET = new String[4];
		String[][] dataGET = new String[numOfIterations][3];
		int randomNodeNumber;
		String randomNode;
		InetAddress serverIP;
		KVClient kvStore;
		
		//Send SET requests
		//Create a client for a random node
		randomNodeNumber = rand.nextInt(nodes.size());
		randomNode = nodes.get(randomNodeNumber);	
		serverIP = InetAddress.getByName(randomNode);
		kvStore = new KVClient(serverIP, serverPort,timeOutms);
		dataSET[0] = randomNode;
		
		//Generate random keys and values
		String randomKey 	= randomString(15);
//			String randomKey 	= "1";
		String randomValue 	= randomString(20);
		dataSET[1] = randomKey;
		dataSET[2] = randomValue;
			
			
			
		//Send request to client
		KVErrors reply = kvStore.set(randomKey, randomValue);
		dataSET[3] = String.valueOf(reply);
			


		//Kill node afte Set requests?
		if(killNode)
		{
			//Remove One Node
			randomNodeNumber = rand.nextInt(nodes.size());
			String deletedNode = nodes.remove(randomNodeNumber);
			//Kill the removed node
			serverIP = InetAddress.getByName(deletedNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			kvStore.kill();
		}
		
		
		//Send GET requests 
		//Create a client for a random node
		randomNodeNumber = rand.nextInt(nodes.size());
		randomNode = nodes.get(randomNodeNumber);	
		serverIP = InetAddress.getByName(randomNode);
		kvStore = new KVClient(serverIP, serverPort,timeOutms);
		for(int i=0; i<numOfIterations; i++)
		{
			//Update Console with Progress
			System.out.println(i + "...");
			
//			//Create a client for a random node
//			randomNodeNumber = rand.nextInt(nodes.size());
//			randomNode = nodes.get(randomNodeNumber);	
//			serverIP = InetAddress.getByName(randomNode);
//			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			
			dataGET[i][0] = randomNode;
			
			//What's the Key?
			String key 	= dataSET[1];

			
			//Send GET request to clients
			long startTime = System.nanoTime();
			String replyGET = null;
			try{
				replyGET = kvStore.get(key);
			}catch(Exception e){
				replyGET = "Exception!";
			}
			long estimatedTime = System.nanoTime() - startTime;
			if(replyGET!= null)
				dataGET[i][1] = replyGET;
			else
				dataGET[i][1] 	= kvStore.errorKV.getMessage();
			dataGET[i][2] 		= String.valueOf(estimatedTime);
			
			//Clear Console
		}
		

		//Print All Captured Data
		//DataSET
		System.out.println("=====================================================================");
		String[] columns = {"Node used for SET", "Key", "Value", "Set Reply"};
		for(int i=0; i<columns.length; i++)
			System.out.print(String.format("%1$-24s\t", columns[i]));
		System.out.println();
		for(int i=0; i<columns.length; i++)
			System.out.print(String.format("%1$-24s\t", dataSET[i]));
		System.out.println();
		System.out.println();
		//DataGET
		String[] columns2 = {"Node used for Get", "GET Reply", "Execution Time"};
		for(int i=0; i<columns2.length; i++)
			System.out.print(String.format("%1$-24s\t", columns2[i]));
		System.out.println();
		for(int i=0; i<numOfIterations; i++)
		{
			for(int j=0; j<3; j++)
				System.out.print(String.format("%1$-24s\t", dataGET[i][j]));
			System.out.println();
		}
		System.out.println("=====================================================================");
	}
	
	
	void testFewGets(String filename, String key, int numOfIterations, boolean killNode) throws IOException
	{
		
		//Get list of nodes
		ArrayList<String> nodes = readListOfNodes(filename);
	
		String[] dataSET = new String[4];
		String[][] dataGET = new String[numOfIterations][3];
		int randomNodeNumber;
		String randomNode;
		InetAddress serverIP;
		KVClient kvStore;
		
//		//Create a client for a random node
//		randomNodeNumber = rand.nextInt(nodes.size());
//		randomNode = nodes.get(randomNodeNumber);	
//		serverIP = InetAddress.getByName(randomNode);
//		kvStore = new KVClient(serverIP, serverPort,timeOutms);
//		dataSET[0] = randomNode;
		

			


		//Kill node afte Set requests?
		if(killNode)
		{
			//Remove One Node
			randomNodeNumber = rand.nextInt(nodes.size());
			String deletedNode = nodes.remove(randomNodeNumber);
			//Kill the removed node
			serverIP = InetAddress.getByName(deletedNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			kvStore.kill();
		}
		
		
		//Send GET requests 
		//Create a client for a random node
		randomNodeNumber = rand.nextInt(nodes.size());
		randomNode = nodes.get(randomNodeNumber);	
		serverIP = InetAddress.getByName(randomNode);
		kvStore = new KVClient(serverIP, serverPort,timeOutms);
		for(int i=0; i<numOfIterations; i++)
		{
			//Update Console with Progress
			System.out.println(i + "...");
			
			//Create a client for a random node
			randomNodeNumber = rand.nextInt(nodes.size());
			randomNode = nodes.get(randomNodeNumber);	
			serverIP = InetAddress.getByName(randomNode);
			kvStore = new KVClient(serverIP, serverPort,timeOutms);
			
			dataGET[i][0] = randomNode;
			
			//What's the Key?
			//key

			
			//Send GET request to clients
			long startTime = System.nanoTime();
			String replyGET = null;
			try{
				replyGET = kvStore.get(key);
			}catch(Exception e){
				replyGET = "Exception!";
			}
			long estimatedTime = System.nanoTime() - startTime;
			if(replyGET!= null)
				dataGET[i][1] = replyGET;
			else
				dataGET[i][1] 	= kvStore.errorKV.getMessage();
			dataGET[i][2] 		= String.valueOf(estimatedTime);
			
			//Clear Console
		}
		

		//Print All Captured Data
		System.out.println("=====================================================================");
		//DataGET
		String[] columns2 = {"Node used for Get", "GET Reply", "Execution Time"};
		for(int i=0; i<columns2.length; i++)
			System.out.print(String.format("%1$-24s\t", columns2[i]));
		System.out.println();
		for(int i=0; i<numOfIterations; i++)
		{
			for(int j=0; j<3; j++)
				System.out.print(String.format("%1$-24s\t", dataGET[i][j]));
			System.out.println();
		}
		System.out.println("=====================================================================");
	}
	
	



	static String randomString( int len ) 
	{
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	static ArrayList<String> readListOfNodes(String filename) throws IOException
	{
		ArrayList<String> nodes = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		while ((line = br.readLine()) != null) {
		   // process the line.
			nodes.add(line);
		}
		br.close();
		return nodes;
	}



}
