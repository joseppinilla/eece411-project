package com.group8.eece411.KVStore;

public enum KVCmds {
	NOCMD(0),
	SET(1), 
	GET(2), 
	REMOVE(3), 
	KILL(4),
	DELNODE(5),
	JOINNODE(6),
	REPLICATE(7),
	GETREPLICA(8),
	REMREPLICA(9),
	PING(10)
	;

	private final int cmd;
	
	private KVCmds(int id){
		this.cmd = id;
	}

	public byte getByte() {
		return (byte) cmd;
		
	}

	public static KVCmds valueOf(byte cmdByte) {
		return KVCmds.values()[cmdByte];
	}
	

}
