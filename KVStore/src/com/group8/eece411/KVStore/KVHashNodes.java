package com.group8.eece411.KVStore;

import java.awt.List;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

/*
 * 
 */
public class KVHashNodes {

	private NavigableMap<Integer,String> navigableMap;
	private ArrayList<String> fullNodesList;
	public static int myHash;
	private int hashMod = 100;
	int numOfSuccessors;
	
	public static void main(String[] args) throws IOException {
		testDistribution("test/assi5Nodes/assi5Nodes_1.txt");
		testDistribution("test/assi5Nodes/assi5Nodes_2.txt");
		testDistribution("test/assi5Nodes/assi5Nodes_3.txt");
//		testDistribution("test/assi4Nodes/assi4Nodes_4.txt");
//		testDistribution("test/assi4Nodes/assi4Nodes_5.txt");
//		testDistribution("test/assi4Nodes/assi4Nodes_50.txt");

	}
	
	static void testDistribution(String filename) throws IOException
	{
		KVHashNodes Test = new KVHashNodes(filename, 2);
		
//		System.out.println("heeeere" + Test.getNodeHostname("ZBWY3VZK8HGPQ5U"));
		
		//Initialize Distribution Measurement
		Hashtable dist = new Hashtable<String, Double>();
		for(String key: Test.navigableMap.values())
			dist.put(key, 0);

		//Read a Dataset, and measure distribution
		FileInputStream fstream = new FileInputStream("test/KVLargeData.csv");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		int numberOfTestKeys = 0;
		while ((strLine = br.readLine()) != null)   {
			String key = Test.getNodeHostname(strLine);
			int occurance = (int) dist.get(key);
			occurance++;
			dist.put(key, occurance);
//			System.out.println(occurance);
			numberOfTestKeys++;
		}
		br.close();
		
		
		//Show Distribution Results
		System.out.println (dist.entrySet());
		Set set = dist.entrySet();
	    Iterator it = set.iterator();
	    while (it.hasNext())
		{
	    	Map.Entry entry = (Map.Entry) it.next();
	    	int Value = (int)entry.getValue();
//			System.out.println(entry.getKey() + " :" + ((double) Value / numberOfTestKeys * 100) + "%");
			System.out.println(((double) Value / numberOfTestKeys * 100) + "%");

		}
	}

	//The constructor just calls the initialize() function
	public KVHashNodes(int numOfSuccessors) throws IOException
	{
		this.numOfSuccessors = numOfSuccessors;
		initialize("test/assi4Nodes/assi4Nodes_1.txt");
	}
	
	public KVHashNodes(String filename, int numOfSuccessors) throws IOException
	{
		this.numOfSuccessors = numOfSuccessors;
		initialize(filename);
	}
	
	
	/*initialize() reads the list of participating nodes, and puts them in a NavigableMap
	* The NavigableMap: Key = hashcode() of Hostname. Value = Hostname.
	* VNavigableMap is used instead of Hashtable, because it has the floorKey() function
	* THEN, add all values in navigableMap to fullNodesList, which will keep nodes even if killed/removed
	*/
	public void initialize(String filename) throws IOException
	{
		if(KVServerRun.DEV_MODE)
			System.out.println("Enter initialize()");
		//NavigableMap
		navigableMap=new TreeMap<Integer, String>();
//		URL url = this.getClass().getResourceAsStream("serviceNodes.txt");
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		while ((line = br.readLine()) != null) {
		   // process the line.
			navigableMap.put(line.hashCode()%hashMod, line);
		}
		br.close();
		
		//Add all nodes from navigableMap to fullNodeList ArrayList
		fullNodesList = new ArrayList<String>();
		for(Entry<Integer, String> entry: navigableMap.entrySet()) // iterate key/value entries.
		{
//			System.out.println(entry.getKey() + ", " + entry.getValue());
			fullNodesList.add(entry.getValue());
		}
		
		myHash = getMyHostname().hashCode();
		System.out.println(getNodeInetAddress("1"));
		
		if(KVServerRun.DEV_MODE)
			System.out.println("Exit initialize()");
	}
	
	/* Input: String Key
	 * Return: String Hostname that holds the Key
	 * To make the map behave as a ring: if the returned entry is null (meaning hashedKey < smallest Key), return the hostname of the highest key
	 */
	public String getNodeHostname(String Key)
	{
		int hashedKey = Key.hashCode()%hashMod;
//		System.out.println(hashedKey);
		//Find the key smaller than hashedKey. entry's value is the hostname
		Entry<Integer, String> entry = navigableMap.floorEntry(hashedKey);
		
		//Ring
		if(entry == null)
		{
			if(KVServerRun.DEV_MODE)
				System.out.println("getNodeHostname() returns: " + navigableMap.lastEntry().getValue());
			return navigableMap.lastEntry().getValue();
		}
		
		if(KVServerRun.DEV_MODE)
			System.out.println("getNodeHostname() returns: " + entry.getValue());
		return entry.getValue();
	}
	
	//Returns hostname (for example: planetlab1.dojima.wide.ad.jp)
	public static String getMyHostname() throws UnknownHostException
	{
		return java.net.InetAddress.getLocalHost().getHostName();
	}
	
	//Check if Key belongs to my hostname
	public boolean isKeyLocal(String Key) throws UnknownHostException
	{
		if(getNodeHostname(Key).equals(getMyHostname()))
		{
			if(KVServerRun.DEV_MODE)
			{
				System.out.println("isKeyLocal() is comparing " + getNodeHostname(Key) + " with " + getMyHostname());
				System.out.println("isKeyLocal() returns: true");
			}
			return true;
		}
		
		if(KVServerRun.DEV_MODE)
		{
			System.out.println("isKeyLocal() is comparing " + getNodeHostname(Key) + " with " + getMyHostname());
			System.out.println("isKeyLocal() returns: false");
		}
			
		return false;
	}
	
	/**
	 * doesKeyBelongToHostname
	 * get hostname Inet, 
	 * find keyBelongsTo what node, get its Inet
	 * Compare both values
	 * @param key of interest
	 * @param hostname of node of interest
	 * @return True if key is in range of hostname
	 * @throws UnknownHostException
	 */
	public boolean doesKeyBelongToHostname(String key, String hostname) throws UnknownHostException{
		//Find where the key belongs
		InetAddress keyBelongsTo 	= getNodeInetAddress(key);
		
		//Get HOSTNAME Info
		InetAddress hostnameInet 	= InetAddress.getByName(hostname);
	
		//doesKeyBelongToHostname?
		return keyBelongsTo.equals(hostnameInet);
	}
	
	/**
	 * Check if key is out of range of this node in order to remove it
	 * We check if the key belongs to either the Current node, or any of its PREDECESSORS
	 * @param key of interest
	 * @return True if key is removable
	 * @throws IOException 
	 */
	public boolean isKetOutOfRange(String key) throws IOException{
		//Find where the key belongs
		InetAddress keyBelongsToInet	= getNodeInetAddress(key);
		
		//Get myHostname Info
		InetAddress myHostname = InetAddress.getByName(getMyHostname());
		
		//Create and ArrayList of current node + getPredecessorsInetAddresses
		ArrayList<InetAddress> myRange = getPredecessorsInetAddresses();
		myRange.add(myHostname);
		
		//iS Key Out Of Range?
		return myRange.contains(keyBelongsToInet);
	}	
	

	
	
	//Return hostname as InetAddress 
	public InetAddress getNodeInetAddress(String Key)
	{
		String hostname = getNodeHostname(Key);
		try {
			if(KVServerRun.DEV_MODE)
			{
//				System.out.println("getNodeInetAddress() returns " + new String(InetAddress.getByName(hostname).getAddress()));
				System.out.println("getNodeInetAddress() returns Inet of " + hostname);
			}
			return InetAddress.getByName(hostname);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*Delete node from the Navigable Map
	 * This is done in two steps:
	 * First: find the Hostname this key belongs to.
	 * Second: Delete the hostname from the NavigableMap
	 * remove(hashedKey). hashedKey is the hashcode() for the hostname
	 */
	public void deleteNodeByKey(String Key)
	{
		int hashedKey = getNodeHostname(Key).hashCode()%hashMod;
		navigableMap.remove(hashedKey);
	}
	
	/*Delete node from the Navigable Map
	 * Delete the hostname from the NavigableMap
	 * remove(hashedKey). hashedKey is the hashcode() for the hostname
	 */
	public String deleteNodeByHostname(String hostname) {
		int hashedKey = hostname.hashCode()%hashMod;
		return navigableMap.remove(hashedKey);	
	}
	
	/*Join node to the Navigable Map
	 * Join the hostname to the NavigableMap
	 * 
	 */
	public String joinNodeByHostname(String hostname)	{
		int hashedKey = hostname.hashCode()%hashMod;
		return navigableMap.put(hashedKey, hostname);
	}

	/**
	 * True if node hostname is Successor in range of number of replicas
	 * @param Host name of node of interest
	 * @return boolean
	 * Change hostname from String to InetAddress, 
	 * then check whether it is included in the list returned from getSuccessorsInetAddresses() 
	 * @throws IOException 
	 */
	public boolean isSuccessor(String hostname) throws IOException{
		InetAddress hostnameInet = InetAddress.getByName(hostname);
		ArrayList<InetAddress> NodesInetAddress = getSuccessorsInetAddresses();
		return NodesInetAddress.contains(hostnameInet);
	}
	
	/**
	 * True if node hostname is Predecessor in range of number of replicas
	 * @param Host name of node of interest
	 * @return boolean
	 * Change hostname from String to InetAddress, 
	 * then check whether it is included in the list returned from getPredecessorsInetAddresses() 
	 * @throws IOException 
	 */
	public boolean isPredecessor(String hostname) throws IOException{
		InetAddress hostnameInet = InetAddress.getByName(hostname);
		ArrayList<InetAddress> NodesInetAddress = getPredecessorsInetAddresses();
		return NodesInetAddress.contains(hostnameInet);
	}

	/**return an arraylist of InetAddress of numOfSuccessors successor nodes.
	 * @param 
	 * @return
	 * @throws IOException
	 */
	public ArrayList<InetAddress> getPredecessorsInetAddresses() throws IOException
	{
				
		ArrayList<InetAddress> NodesInetAddress = new ArrayList<InetAddress>();
		//Find the next 2 Predecessor
		int keyPointerHash = myHash;	//This will be used to keep looking for its ceiling
		int numOfPredecessors = numOfSuccessors;
		for(int i =0; i<numOfPredecessors; i++)
		{
			Entry<Integer, String> entry = navigableMap.floorEntry(keyPointerHash);
			if(entry==null)	//current entry is "first" in the ring
			{
				entry = navigableMap.lastEntry();
			}
			
			NodesInetAddress.add(InetAddress.getByName(entry.getValue()));
			keyPointerHash = entry.getKey() - 1;
		}
		return NodesInetAddress;
	}
	
	/**return an arraylist of InetAddress of numOfSuccessors successor nodes.
	 * @param 
	 * @return
	 * @throws IOException
	 */
	public ArrayList<InetAddress> getSuccessorsInetAddresses() throws IOException
	{
				
		ArrayList<InetAddress> NodesInetAddress = new ArrayList<InetAddress>();
		//Find the next 2 successors
		int keyPointerHash = myHash;	//This will be used to keep looking for its ceiling
		for(int i =0; i<numOfSuccessors; i++)
		{
			Entry<Integer, String> entry = navigableMap.ceilingEntry(keyPointerHash);
			if(entry==null)	//current entry is "last" in the ring
			{
				entry = navigableMap.firstEntry();
			}
			
			NodesInetAddress.add(InetAddress.getByName(entry.getValue()));
			keyPointerHash = entry.getKey() + 1;
		}
		return NodesInetAddress;
	}
	
	
	/**
	 * Called when a succesor node has been deleted to get next successor 
	 * @return InetAddress of next successor
	 * @throws IOException 
	 */
	public InetAddress getNewSuccessorInetAddress() throws IOException{
		//getSuccessorsInetAddresses() returns an ArrayList of numOfSuccessors' elements.
		//What we want is the last element (index = numOfSuccessors)
		ArrayList<InetAddress> NodesInetAddress = getSuccessorsInetAddresses();
		return NodesInetAddress.get(numOfSuccessors);
		
	}
	
	
	/* Returns size of navigableMap
	 */
	public int getSize()
	{
		return navigableMap.size();
	}
	
	/* Returns all values (hostnames)
	 */
	public Collection<String> getHostnames()
	{
		return navigableMap.values();
	}
	
	/* Returns all values (hostname) as InetAddress, in an arraylist
	 */
	public ArrayList<InetAddress> getNodesInetAddress() throws UnknownHostException
	{
		ArrayList<InetAddress> NodesInetAddress = new ArrayList<InetAddress>();
		
		//InetAddress.getByName
		for(String value: navigableMap.values()) 
		{
			System.out.println(value);
			NodesInetAddress.add(InetAddress.getByName(value));
		}

		return NodesInetAddress;
	}
	
	/* Returns random X values (hostname) as InetAddress, in an arraylist
	 */
	public ArrayList<InetAddress> getRandomMembersInetAddress(int X) throws UnknownHostException
	{
		ArrayList<InetAddress> NodesInetAddress 		= getNodesInetAddress();
		ArrayList<InetAddress> RandomNodesInetAddress	= new ArrayList<InetAddress>();
		
		//Pick X random elements from the Nodes list
		Random rand = new Random();
		InetAddress myInetHostname = InetAddress.getByName(getMyHostname());
		//Try X*2 times
		for(int i=0; i<X*3; i++) 
		{
			int randomNodeNumber 	= rand.nextInt(NodesInetAddress.size());
			InetAddress RandomNode	= NodesInetAddress.get(randomNodeNumber);
			if(!RandomNode.equals(myInetHostname))
				RandomNodesInetAddress.add(NodesInetAddress.get(randomNodeNumber));
			if(RandomNodesInetAddress.size() >= X)
				break;
		}

		return RandomNodesInetAddress;

	}
	
	/**
	 * A random node is selected to check for membership, from fullNodesList
	 * @return InetAddress of Random Node
	 * @throws IOException 
	 * Never return myself
	 */
	public InetAddress getRandomNodeInetAddress() throws IOException{
		InetAddress RandomNodeInetAddress = null;
		
		//Try to find a random node 10 times
		for(int i=0; i<10; i++)
		{
			Random rand = new Random();
			int randomNodeNumber 	= rand.nextInt(fullNodesList.size());
			String RandomNode		= fullNodesList.get(randomNodeNumber);
			
			if(!RandomNode.equals(getMyHostname()))
				return InetAddress.getByName(RandomNode);
		}

		return null;
	}
	
}
