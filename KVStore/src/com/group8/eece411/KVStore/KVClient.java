package com.group8.eece411.KVStore;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Random;

public class KVClient {

	private static ByteBuffer reqBuf;
	private static ByteBuffer repBuf;
	/*
	+=====+=====+=====+=========+=========+
	| HDR | CMD | KEY | VLEN(*) |   VAL   |
	+=====+=====+=====+=========+=========+
	| 16B | 1B  | 32B |   2B    | <15000B |
	+-----+-----+-----+---------+---------+
	(*)Little Endian
	*/
	
	private static final int HDR_SIZE = 16;
	private static final int CMD_SIZE = 1;
	private static final int KEY_SIZE = 32;
	private static final int VLEN_SIZE = 2;
	private static final int MAX_VAL = 15000;
	
	private static final int CMD_INDEX = HDR_SIZE;
	private static final int KEY_INDEX = CMD_INDEX+CMD_SIZE;
	private static final int VLEN_INDEX = KEY_INDEX+KEY_SIZE;
	private static final int VAL_INDEX = VLEN_INDEX+VLEN_SIZE;
	
	
	private static final int TIMEOUT_INC = 2;
	private static final int RETRY_NUM = 4;	
	
	private int REQ_SIZE = HDR_SIZE+CMD_SIZE+KEY_SIZE+VLEN_SIZE+MAX_VAL;
	private int REP_SIZE = HDR_SIZE+CMD_SIZE+KEY_SIZE+VLEN_SIZE+MAX_VAL;

	byte [] repID;
	
	private InetAddress serverInetAddress;
	private int serverPort;
	private int timeOutms;

	KVErrors errorKV;
	
	/** 
	 * KVStore constructor, configurable retry timeout
	 * Fill-in UDP Packet Header ID
	 * Unique ID[16]: |hostIP3|...|hostIP0|Port1|Port0|Rand1|Rand0|miliSec7|...|miliSec0|
	 */
	public KVClient(InetAddress serverInetAddress, int serverPort, int timeOutms){
	
		/* Configure datagram parameters
		 */
		this.serverInetAddress = serverInetAddress;
		this.serverPort = serverPort;
		this.timeOutms = timeOutms;
	}
	
	/** 
	 * KVStore constructor, default retry timeout 100ms
	 * Fill-in UDP Packet Header ID
	 * Unique ID[16]: |hostIP3|...|hostIP0|Port1|Port0|Rand1|Rand0|miliSec7|...|miliSec0|
	 */
	public KVClient(InetAddress serverInetAddress, int serverPort){
		this(serverInetAddress, serverPort, 100);
	}
	
	
	/** Fill request buffer with unique ID
	 */
	public void initBuf(){
		/* Allocate Buffer
		 */
		reqBuf = ByteBuffer.allocate(REQ_SIZE);
		repBuf = ByteBuffer.allocate(REP_SIZE);
		//All Integers get put in little endian order.
		//When copying byte arrays these are copied "as is"
		reqBuf.order(ByteOrder.LITTLE_ENDIAN);
		repBuf.order(ByteOrder.LITTLE_ENDIAN);
		
		repID = new byte[HDR_SIZE];
		
		errorKV = KVErrors.OK;
		
		byte[] hostIP = KVClient.getHostIP().getAddress();
		Random rand = new Random();
		
		reqBuf.put(hostIP);
		reqBuf.putShort((short)serverPort);		
		reqBuf.putShort((short)rand.nextInt(65535));
		reqBuf.putLong(System.currentTimeMillis());
	}
	
	/** 
	 * Set new server to use same client in multiple requests
	 */
	public void setServer(InetAddress serverInetAddress, int serverPort){
		this.serverInetAddress=serverInetAddress;
		this.serverPort=serverPort;
	}	
		
	/** 
	 * Read list of Network Interfaces and return device IPv4
	 * From project Android-Activity-Tracker.
	 * 
	 * @return the IP address of the device. 
	 */
	public static InetAddress getHostIP(){
		try {
			for (Enumeration<NetworkInterface> en=NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
				NetworkInterface intf=en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr=intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
					InetAddress inetAddress=enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress() && (inetAddress instanceof Inet4Address)) {
						return inetAddress;
					}
				}
			}
		} catch (  SocketException ex) {
			System.err.println("Error getting IP");
		}
		return null;
	}	

	/** 
	 * Set data command on KV
	 * @param Key to set value
	 * @param Value to set to Key
	 *   
	 */
	public KVErrors set(String reqKey, String setValue) {
		
		if(KVClientTest.DEV_MODE){
			System.out.println("SET REQUEST: " + reqKey + " " + setValue);
		}
		
		initBuf();
		reqBuf.put(KVCmds.SET.getByte());
		//Put Key
		reqBuf.put(reqKey.getBytes());
		reqBuf.position(VLEN_INDEX);
		//Put Value-Length
		reqBuf.putShort((short)setValue.length());
		//Put Value
		reqBuf.put(setValue.getBytes());
		return sendPublicRequest();
	}
	
	/** 
	 * Get data command from KV
	 * @param Key to get value
	 *   
	 */
	public String get(String reqKey) {
		initBuf();
		reqBuf.put(KVCmds.GET.getByte());
		//Put Key
		reqBuf.put(reqKey.getBytes());
		reqBuf.position(VLEN_INDEX);
		errorKV = sendPublicRequest();
		
		if (errorKV.isOK()){
			return parseGet();
		}
		else{
			System.err.println(errorKV.getMessage());
			return null;
		}
	}
	
	/** 
	 * From Reply Buffer return value string
	 * @return Value String
	 */
	public String parseGet() {
		short valLength = repBuf.getShort();
		if(KVClientTest.DEV_MODE){
			System.out.println("Length: " + valLength);
		}
		byte[] repVal = new byte[MAX_VAL];
		repBuf.get(repVal,0,(int)valLength);
		return new String(Arrays.copyOfRange(repVal,0,valLength));
	}

	/** 
	 * Remove data command from KV
	 * @param Key to remove
	 * @return Error code <KVErrors>
	 *   
	 */
	public KVErrors remove(String reqKey) {
		initBuf();
		reqBuf.put(KVCmds.REMOVE.getByte());
		//Put Key
		reqBuf.put(reqKey.getBytes());
		return sendPublicRequest();
	}

	/** 
	 * Send Gossip Comm
	 *   
	 */
	public void initGossip(String node, KVCmds cmd, int serverPort) {
		initBuf();
		//Put request command
		reqBuf.put(cmd.getByte());
		//Put Request ID {TimeStamp,Own Hash}
		long idReq = (long) KVHashNodes.myHash;
		idReq = idReq | (System.currentTimeMillis()<<4);
		reqBuf.putLong(idReq);
		//Put Hostname Length
		reqBuf.putShort((short)node.length());
		//Put node hostname as value
		reqBuf.put(node.getBytes());	
	}
	
	/** 
	 * Kill node 
	 */
	public KVErrors kill() {
		initBuf();
		reqBuf.put(KVCmds.KILL.getByte());
		return sendPublicRequest();
	}
	
	/** 
	 * Ping node
	 */
	public KVErrors ping() {
		initBuf();
		reqBuf.put(KVCmds.PING.getByte());
		return sendInternalRequest();
	}

	/** 
	 * Replicate Key, Value
	 */
	public KVErrors replicate(String reqKey, short valLength, String reqVal) {
		initBuf();
		//Put request command
		reqBuf.put(KVCmds.REPLICATE.getByte());
		//Put Key
		reqBuf.put(reqKey.getBytes());
		reqBuf.position(VLEN_INDEX);
		//Put Value-Length
		reqBuf.putShort(valLength);
		//Put Value
		reqBuf.put(reqVal.getBytes());
		return sendInternalRequest();	
	}
	
	/** 
	 * Get Replicated Key Values
	 */
	public String getReplica(String reqKey) {
		initBuf();
		reqBuf.put(KVCmds.GETREPLICA.getByte());
		//Put Key
		reqBuf.put(reqKey.getBytes());
		errorKV = sendInternalRequest();
		
		if (errorKV.isOK()){
			return parseGet();
		}
		else{
			System.err.println(errorKV.getMessage());
			return null;
		}
	}
	
	/** 
	 * Remove Replicated Key Values
	 */
	public KVErrors removeReplica(String reqKey) {
		initBuf();
		reqBuf.put(KVCmds.REMREPLICA.getByte());
		//Put Key
		reqBuf.put(reqKey.getBytes());
		return sendInternalRequest();
		
	}
	
	
	public void setTimeOut(int timeOutms){
		this.timeOutms = timeOutms;
	}
	
	public void setRepBufSize(int repSize){
		this.REP_SIZE = repSize;
	}
	
	public void setReqBufSize(int reqSize){
		this.REQ_SIZE = reqSize;
	}
	
	
	public void fwdNonBlockingRequest(byte[] reqArray){

		// Allocate Buffer
		reqBuf = ByteBuffer.allocate(REQ_SIZE);
		
		reqBuf.put(reqArray);
		
		sendNonBlockingRequest();
	}
	
	public KVErrors fwdRequest(byte[] reqArray){

		// Allocate Buffer
		reqBuf = ByteBuffer.allocate(REQ_SIZE);
		repBuf = ByteBuffer.allocate(REP_SIZE);
		
		repBuf.order(ByteOrder.LITTLE_ENDIAN);
		
		repID = new byte[HDR_SIZE];
		
		reqBuf.put(reqArray);
		
		return sendInternalRequest();
	}
	
	public KVErrors sendPublicRequest() {
		
		DatagramSocket clientSocket = null;
		
		int i=1;
		
		while (clientSocket==null && (i<10)){
			try {
				clientSocket = new DatagramSocket();
			} catch (SocketException e) {
				if(KVClientTest.DEV_MODE){
					System.err.println("Failed Opening Public Socket");
				}
			}
			i++;
		}
		
		sendRequest(clientSocket);
		
		clientSocket.close();
		

		return this.errorKV;	
	}
	
	
	public KVErrors sendInternalRequest() {
		
		DatagramSocket clientSocket = null;
		int i=1;
		
		while (clientSocket==null && (i<10)){
			try {
				clientSocket = new DatagramSocket(this.serverPort+i);
			} catch (SocketException e) {
				if(KVClientTest.DEV_MODE){
					System.err.println("Failed Opening Internal Socket");
				}
			}
			i++;
		}
		if (clientSocket!=null){

			sendRequest(clientSocket);

			clientSocket.close();

			return errorKV;			
		}
		errorKV = KVErrors.INTERNAL_ERROR;
		
		return errorKV;
	}
	
	public void sendNonBlockingRequest() {
		
		DatagramSocket clientSocket = null;
		int i=1;
		
		while (clientSocket==null && (i<10)){
			try {
				clientSocket = new DatagramSocket(this.serverPort+i);
			} catch (SocketException e) {
				System.err.println("Failed Opening Non Blocking Socket");
			}
			i++;
		}
		
		if (clientSocket==null){
			return;
		}
		
		DatagramPacket sendPacket = new DatagramPacket(reqBuf.array(),reqBuf.position(), this.serverInetAddress, this.serverPort);

		if(KVClientTest.DEV_MODE){
			System.out.println("SIZE OF REQUEST " + reqBuf.position());
			System.out.println("PORT OF REQUEST " + this.serverPort);
			System.out.println("REQUEST: " + Arrays.toString(Arrays.copyOfRange(reqBuf.array(),0,reqBuf.position())));
		}
		
		/*-------------------------Send-------------------------------*/
		try {
			clientSocket.send(sendPacket);
		} catch (Exception e) {
			if(KVClientTest.DEV_MODE){
				System.err.println("Failed Sending Packet");
			}
			
		}

		clientSocket.close();
	}

	
	/** 
	 * Send UDP Packet, Receive UDP Reply and Verify ID
	 * @param timeOutms configurable retry timeout in milliseconds
	 * @return Boolean Matching ID for response and request 
	 *  
	 */
	public KVErrors sendRequest(DatagramSocket clientSocket) {

		int timeOutms = this.timeOutms;
		
		byte[] repBytes = new byte[REP_SIZE];

		DatagramPacket receivePacket = new DatagramPacket(repBytes,REP_SIZE);
		boolean matchReqRep = false;
		int retryCnt = 0;
		DatagramPacket sendPacket = new DatagramPacket(reqBuf.array(),reqBuf.position(), this.serverInetAddress, this.serverPort);

		if(KVClientTest.DEV_MODE){
			System.out.println("SIZE OF REQUEST " + reqBuf.position());
			System.out.println("PORT OF REQUEST " + this.serverPort);
			System.out.println("REQUEST: " + Arrays.toString(Arrays.copyOfRange(reqBuf.array(),0,reqBuf.position())));
		}
		
		do{
			/*-------------------------Send-------------------------------*/
			try {
				clientSocket.send(sendPacket);
			} catch (Exception e) {
				if(KVClientTest.DEV_MODE){
					System.err.println("Failed Sending Packet");
				}
				errorKV = KVErrors.INTERNAL_ERROR;
				return errorKV;
			}

			/*------------------------Receive-----------------------------*/
			try {
				clientSocket.setSoTimeout(timeOutms);
			} catch (SocketException e1) {
				if(KVClientTest.DEV_MODE){
					System.err.println("Failure Setting Socket");
				}
				errorKV = KVErrors.INTERNAL_ERROR;
				return errorKV;
			}
			
			repBuf.clear();
			
			try{
				clientSocket.receive(receivePacket);
				repBuf.put(Arrays.copyOfRange(receivePacket.getData(),0,receivePacket.getLength()));
			} catch(Exception e){
				if(KVClientTest.DEV_MODE){
					System.err.println("Server not responding. Retrying(" + retryCnt +")...");
				}
				errorKV = KVErrors.INTERNAL_ERROR;
				return errorKV;
			}
			
			if(KVClientTest.DEV_MODE){
				System.out.println("SIZE OF REPLY " + repBuf.position());
				System.out.println("REPLIED: " + Arrays.toString(Arrays.copyOfRange(repBuf.array(),0,repBuf.position())));
			}

			/*---------------------Match Unique ID------------------------*/
			
			if (repBuf.position()!=0){
				repBuf.flip();
				repBuf.get(repID,0,HDR_SIZE);
				matchReqRep = Arrays.equals(Arrays.copyOfRange(reqBuf.array(), 0, HDR_SIZE), repID);
			}
			/*--------------Increment Retry and Timeout-------------------*/
			retryCnt++;
			timeOutms *= TIMEOUT_INC;
			
		}while(matchReqRep==false && retryCnt<RETRY_NUM);

		//If finished timeout
		if (!matchReqRep){
			errorKV = KVErrors.INTERNAL_ERROR;
		}
		else{
			if(KVClientTest.DEV_MODE){
				System.out.println("ID MATCH!");
			}
			//Read response code
			errorKV = KVErrors.valueOf(repBuf.get());			
		}

		return errorKV;	
	}

}

