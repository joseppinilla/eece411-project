package com.group8.eece411.KVStore;

import static java.util.Arrays.asList;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

/* UDP Test Server
 * Program acts as server and echoes UDP packets
 * Server never closes, testing only.
 * */

public class KVServerRun {
	
	public static final boolean DEV_MODE = true;
	
	public static final boolean DEPLOY_MODE = false;
	
	public static void main(String args[]) throws Exception {
			
		/* Define Options
		 * */
		OptionParser parser = new OptionParser();
		parser.acceptsAll( asList( "h", "?" ), "Show help" ).forHelp();
		OptionSpec<Short> port = parser.accepts( "p", "Port of service" ).withRequiredArg().ofType(Short.class).describedAs("Port").defaultsTo((short)5628);
		OptionSet options = parser.parse(args);
		
		/* Verify Inputs
		 * */
		if (options.has("h")){
			parser.printHelpOn (System.out);
			System.exit(0);
		}
		
		/* Configure KV and run server
		 * */
		KVServer kvStore = new KVServer(options.valueOf(port));
		kvStore.runServer();


	}
}	